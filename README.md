# SHACL shapes

This repository has [SHACL] shapes, a very simplified [RDFS] ontology and
some test data that comes from museum data.

[SHACL]: https://www.w3.org/TR/shacl/
[RDFS]: https://www.w3.org/TR/rdf-schema/

## Usage

I found [pySHACL] useful and working and call it like this:

```sh
pyshacl -s all-shapes.ttl -e ontology.ttl -i both --imports -f human data/test2.ttl
```

To save validation output to a Turtle file:
```sh
pyshacl -s all-shapes.ttl -e ontology.ttl -i both --imports -f turtle -o validation_result.ttl data/test2.ttl
```
[pySHACL]: https://github.com/RDFLib/pySHACL
